package com.xzy.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 新卓越项目一组
 * @since 2019-05-28
 */
public class Permission extends Model<Permission> {

    private static final long serialVersionUID = 1L;

	private Integer id;
    /**
     * 权限名
     */
	private String name;
    /**
     * 权限对应的uri
     */
	private String uri;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

}
