package com.xzy.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 新卓越项目一组
 * @since 2019-05-28
 */
@TableName("role_per")
public class RolePer extends Model<RolePer> {

    private static final long serialVersionUID = 1L;

	private Integer roleid;
	private Integer perid;


	public Integer getRoleid() {
		return roleid;
	}

	public void setRoleid(Integer roleid) {
		this.roleid = roleid;
	}

	public Integer getPerid() {
		return perid;
	}

	public void setPerid(Integer perid) {
		this.perid = perid;
	}

	@Override
	protected Serializable pkVal() {
		return null;
	}
}
