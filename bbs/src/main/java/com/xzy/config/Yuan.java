package com.xzy.config;

public class Yuan {
    public String name;
    public int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Yuan(String name, int age) {
        this.name = name;
        this.age = age;
    }
    public void init(){
        System.out.println("测试初始化几次");
    }
}
