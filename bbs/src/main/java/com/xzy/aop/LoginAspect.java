package com.xzy.aop;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoginAspect {


    @Pointcut("execution(public * com.xzy.web.*.*(..))")
    public void LogAspect(){}

    @Before("LogAspect()")
    public void before(){
        System.out.println("我是快乐的小青蛙");
    }

    @After("LogAspect()")
    public void after(){
        System.out.println("你的快乐的大猪头");
    }

}
