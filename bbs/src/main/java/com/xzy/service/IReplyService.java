package com.xzy.service;

import com.xzy.entity.Reply;
import com.baomidou.mybatisplus.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 新卓越项目一组
 * @since 2019-05-28
 */
public interface IReplyService extends IService<Reply> {
    /**
     * 根据文章id返回所有评论
     * @param id
     * @return
     */
    public List<Reply> selectAllByArticleId(Integer id);


    public void addReply(Integer articleid,String replyer,Date date,String reply);
}
