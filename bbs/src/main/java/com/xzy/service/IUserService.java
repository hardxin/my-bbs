package com.xzy.service;

import com.github.pagehelper.Page;
import com.xzy.entity.Role;
import com.xzy.entity.User;
import com.baomidou.mybatisplus.service.IService;
import com.xzy.entity.UserRole;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 新卓越项目一组
 * @since 2019-05-28
 */
public interface IUserService extends IService<User> {

    public User selectUserByTel(String tel);

    public void addUser(User user);

    public User selectUser(String tel,String password);

    public Page<User> selectAll();
}
