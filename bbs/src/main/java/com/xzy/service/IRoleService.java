package com.xzy.service;

import com.github.pagehelper.Page;
import com.xzy.entity.Role;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 新卓越项目一组
 * @since 2019-05-28
 */
public interface IRoleService extends IService<Role> {


    public Role selectById(Integer id);

    public Page<Role> selectAll();

    public List<Role> selectAllRole();
}
