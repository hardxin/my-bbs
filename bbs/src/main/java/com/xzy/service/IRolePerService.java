package com.xzy.service;

import com.xzy.entity.RolePer;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 新卓越项目一组
 * @since 2019-05-28
 */
public interface IRolePerService extends IService<RolePer> {


    public List<Integer> selectPermission(Integer roleid);
}
