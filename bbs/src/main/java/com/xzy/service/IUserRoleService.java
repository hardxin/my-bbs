package com.xzy.service;

import com.xzy.entity.UserRole;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 新卓越项目一组
 * @since 2019-05-28
 */
public interface IUserRoleService extends IService<UserRole> {

    public UserRole selectByUserId(int userid);
}
