package com.xzy.service;

import com.xzy.entity.Article;
import com.baomidou.mybatisplus.service.IService;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 新卓越项目一组
 * @since 2019-05-28
 */
public interface IArticleService extends IService<Article> {

   public List<Article> selectAllArticle();

   public List<Article> selectAllArticleByUserId(Integer userid);

   public Article selectArticleById(Integer id);

   public void  addArticle(Integer userid,String title,String author,Date date,String content);

   public void deleteArticle(Integer id);
}
