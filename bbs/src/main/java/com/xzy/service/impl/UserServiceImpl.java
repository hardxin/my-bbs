package com.xzy.service.impl;

import com.github.pagehelper.Page;
import com.xzy.entity.User;
import com.xzy.mapper.UserMapper;
import com.xzy.mapper.UserRoleMapper;
import com.xzy.service.IUserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 新卓越项目一组
 * @since 2019-05-28
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {


    @Autowired
    UserMapper userMapper;

    @Autowired
    UserRoleMapper userRoleMapper;
    @Override
    public User selectUserByTel(String tel) {
        return userMapper.selectUserByTel(tel);
    }

    @Transactional
    @Override
    public void addUser(User user) {
        userMapper.addUser(user);
        userRoleMapper.addUserRole(user.getId(), 3);
    }

    /**
     * 验证电话号码和密码是否正确
     */
    @Override
    public User selectUser(String tel, String password) {
        return userMapper.selectUser(tel, password);
    }

    @Override
    public Page<User> selectAll() {
        return userMapper.selectAll();
    }
}
