package com.xzy.service.impl;

import com.xzy.entity.RolePer;
import com.xzy.mapper.RolePerMapper;
import com.xzy.service.IRolePerService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 新卓越项目一组
 * @since 2019-05-28
 */
@Service
public class RolePerServiceImpl extends ServiceImpl<RolePerMapper, RolePer> implements IRolePerService {


    @Autowired
    RolePerMapper rolePerMapper;
    @Override
    public List<Integer> selectPermission(Integer roleid) {
        return rolePerMapper.selectPermission(roleid);
    }
}
