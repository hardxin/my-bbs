package com.xzy.service.impl;

import com.xzy.entity.Reply;
import com.xzy.mapper.ReplyMapper;
import com.xzy.service.IReplyService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 新卓越项目一组
 * @since 2019-05-28
 */
@Service
public class ReplyServiceImpl extends ServiceImpl<ReplyMapper, Reply> implements IReplyService {

    @Autowired
    ReplyMapper replyMapper;

    @Override
    public List<Reply> selectAllByArticleId(Integer id) {
        return replyMapper.selectAllByArticleId(id);
    }

    @Transactional
    @Override
    public void addReply(Integer articleid, String replyer, Date date, String reply) {
        replyMapper.addReply(articleid, replyer, date, reply);
    }
}
