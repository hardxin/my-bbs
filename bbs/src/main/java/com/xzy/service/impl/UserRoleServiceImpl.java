package com.xzy.service.impl;

import com.xzy.entity.UserRole;
import com.xzy.mapper.UserRoleMapper;
import com.xzy.service.IUserRoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 新卓越项目一组
 * @since 2019-05-28
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements IUserRoleService {
    @Autowired
    UserRoleMapper userRoleMapper;
    @Override
    public UserRole selectByUserId(int userid) {
        return userRoleMapper.selectByUserId(userid);
    }
}
