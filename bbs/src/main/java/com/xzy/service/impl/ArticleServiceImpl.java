package com.xzy.service.impl;

import com.xzy.entity.Article;
import com.xzy.mapper.ArticleMapper;
import com.xzy.service.IArticleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 新卓越项目一组
 * @since 2019-05-28
 */
@Service
public class ArticleServiceImpl extends ServiceImpl<ArticleMapper, Article> implements IArticleService {

    @Autowired
    ArticleMapper articleMapper;
    @Override
    public List<Article> selectAllArticle() {
        return articleMapper.selectAllArticle();
    }

    /**
     * 根据用户id查询文章
     */
    @Override
    public List<Article> selectAllArticleByUserId(Integer userid) {
        return articleMapper.selectAllArticleByUserId(userid);
    }

    @Override
    public Article selectArticleById(Integer id) {
        return articleMapper.selectArticleById(id);
    }

    @Override
    public void addArticle(Integer userid, String title, String author, Date date, String content) {
        articleMapper.addArticle(userid, title, author, date, content);
    }

    @Override
    public void deleteArticle(Integer id) {
        articleMapper.deleteArticle(id);
    }
}
