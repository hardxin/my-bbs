package com.xzy.service.impl;

import com.github.pagehelper.Page;
import com.xzy.entity.Role;
import com.xzy.mapper.RoleMapper;
import com.xzy.service.IRoleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 新卓越项目一组
 * @since 2019-05-28
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {


    @Autowired
    RoleMapper roleMapper;
    @Override
    public Role selectById(Integer id) {
        return roleMapper.selectById(id);
    }

    @Override
    public Page<Role> selectAll() {
        return roleMapper.selectAll();
    }

    @Override
    public List<Role> selectAllRole() {
        return roleMapper.selectAllRole();
    }
}
