package com.xzy.service.impl;

import com.xzy.entity.Permission;
import com.xzy.mapper.PermissionMapper;
import com.xzy.service.IPermissionService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 新卓越项目一组
 * @since 2019-05-28
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements IPermissionService {
	
}
