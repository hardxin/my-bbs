package com.xzy.service;

import com.xzy.entity.Permission;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 新卓越项目一组
 * @since 2019-05-28
 */
public interface IPermissionService extends IService<Permission> {
	
}
