package com.xzy.util;

import com.xzy.entity.Role;
import com.xzy.entity.RolePer;
import com.xzy.entity.User;
import com.xzy.entity.UserRole;
import com.xzy.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.util.List;

@Component
public class License {

    @Autowired
    IUserService userService;

    @Autowired
    IRoleService roleService;

    @Autowired
    IRolePerService rolePerService;

    @Autowired
    IUserRoleService userRoleService;

    public boolean hasArticlePermission(String permission, User user){
        Integer id=user.getId();
        System.out.println(id);
        UserRole userRole=userRoleService.selectByUserId(id);
        Role role=roleService.selectById(userRole.getRoleid());
        System.out.println(role.getId());
        List<Integer> list1=rolePerService.selectPermission(role.getId());
        List<String> list=ConvertRole.toList(list1);
        for(String s:list)
            System.out.println(s);
        if(list.contains(permission))
            return true;
        return false;

    }
}
