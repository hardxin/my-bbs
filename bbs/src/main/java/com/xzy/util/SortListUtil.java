package com.xzy.util;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class SortListUtil {

    public static final String DESC="desc";
    public static final String ASC="asc";

    /**
     * 对list中的元素进行排序
     */
    @SuppressWarnings("unchecked")
    public static List<?> sort(List<?> list,final String field,final String sort){
        Collections.sort(list,(Object o1,Object o2)->{
            int ret=0;
            try {
                Field f=o1.getClass().getDeclaredField(field);
                f.setAccessible(true);
                Field f1=o1.getClass().getDeclaredField(field);
                f1.setAccessible(true);
                Class<?> type=f.getType();
                if(type==int.class){
                    Integer a=f.getInt(o1);
                    Integer b=f1.getInt(o2);
                    ret=a.compareTo(b);
                }else if(type==Double.class){
                    Date a=(Date) f.get(o1);
                    Date b=(Date)f.get(o2);
                    ret=a.compareTo(b);
                }else if(type==Float.class){
                    Float a=f.getFloat(o1);
                    Float b=f.getFloat(o2);
                    ret=a.compareTo(b);
                }else if(type==Date.class){
                    Date a=(Date) f.get(o1);
                    Date b=(Date)f.get(o2);
                    ret=a.compareTo(b);
                }
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } finally {
            }
            if(sort!=null&&sort.toLowerCase().equals(DESC)){
                return -ret;
            }else
                return ret;
        });
        return list;
    }
}
