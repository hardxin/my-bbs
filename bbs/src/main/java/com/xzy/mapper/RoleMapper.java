package com.xzy.mapper;

import com.github.pagehelper.Page;
import com.xzy.entity.Role;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author 新卓越项目一组
 * @since 2019-05-28
 */
public interface RoleMapper extends BaseMapper<Role> {

    public Role selectById(@Param("id") Integer id);

    public Page<Role> selectAll();

    public List<Role> selectAllRole();
}