package com.xzy.mapper;

import com.xzy.entity.UserRole;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author 新卓越项目一组
 * @since 2019-05-28
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

    public void addUserRole(@Param("userid") Integer userid,
                            @Param("roleid") Integer roleid);

    public UserRole selectByUserId(@Param("userid") Integer userid);
}