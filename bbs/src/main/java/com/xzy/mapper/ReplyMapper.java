package com.xzy.mapper;

import com.xzy.entity.Reply;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author 新卓越项目一组
 * @since 2019-05-28
 */
public interface ReplyMapper extends BaseMapper<Reply> {

    public List<Reply> selectAllByArticleId(@Param("id") Integer id);

    /**
     * 增加评论
     */
    public void addReply(@Param("articleid") Integer articleid, @Param("replyer")String replyer, @Param("date")Date date,@Param("reply")String reply);
}