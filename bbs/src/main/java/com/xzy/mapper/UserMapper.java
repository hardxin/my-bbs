package com.xzy.mapper;

import com.github.pagehelper.Page;
import com.xzy.entity.User;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author 新卓越项目一组
 * @since 2019-05-28
 */
public interface UserMapper extends BaseMapper<User> {


    /**
     * 根据电话号码查询是否存在用户
     */
    public User selectUserByTel(@Param("tel") String tel);

    public void addUser(User user);

    public User selectUser(@Param("tel")String tel,@Param("password")String password);

    public Page<User> selectAll();
}