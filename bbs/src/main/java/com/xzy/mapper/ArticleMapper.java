package com.xzy.mapper;

import com.xzy.entity.Article;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author 新卓越项目一组
 * @since 2019-05-28
 */
public interface ArticleMapper extends BaseMapper<Article> {

    /**
     * 查找所有文章
     * @return
     */
    public List<Article> selectAllArticle();

    public List<Article> selectAllArticleByUserId(@Param("userid") Integer userid);

    public Article  selectArticleById(@Param("id") Integer id);

    /**
     * 增加文章
     */
    public void  addArticle(@Param("userid") Integer userid, @Param("title") String title, @Param("author") String author, @Param("date") Date date, @Param("content") String content);

    /**
     * 删除文章
     */
    public void deleteArticle(@Param("id")Integer id);
}