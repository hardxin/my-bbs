package com.xzy.mapper;

import com.xzy.entity.RolePer;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author 新卓越项目一组
 * @since 2019-05-28
 */
public interface RolePerMapper extends BaseMapper<RolePer> {


    /**
     * 查询用户对应的许可
     */
    public List<Integer> selectPermission(@Param("roleid") Integer roleid);
}