package com.xzy.mapper;

import com.xzy.entity.Permission;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author 新卓越项目一组
 * @since 2019-05-28
 */
public interface PermissionMapper extends BaseMapper<Permission> {

}