package com.xzy.web;

import com.xzy.config.Yuan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@Controller
public class ValidateController {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private Yuan yuan;

    //手机验证码模块
    @RequestMapping(value = "/telValidateUtil",method = RequestMethod.POST)
    @ResponseBody
    public String telValidate(@RequestParam(value = "tel") String tel){
        int validate=(int)(Math.random()*1000000);
        validate=validate+100000;
        Date sendValidateTime=new Date();
        if(!redisTemplate.hasKey(tel)){
            redisTemplate.opsForValue().set(tel, validate,60, TimeUnit.SECONDS);
            String s="敬爱的" + tel + "用户，此次生成的验证码为：" + validate + "。有效期为1分钟！";
            String json="{\"errorCode\":\"01\",\"message\":"+"\""+s+"\"}";
            System.out.println("************************");
            System.out.println("敬爱的"+tel+"用户,此次生成的验证码为"+validate+"有效期只有一分钟");
            System.out.println("************************");
            return json;
        }else{
            System.out.println("一个手机号一分钟只能获取一个验证码！！！");
            String json="{\"errorCode\":\"11\",\"errormessage\":\"一个手机号一分钟只能获取一个验证码！！！\"}";
            return json;
        }
    }
    @GetMapping("/test1")
    public @ResponseBody String test(){
        return yuan.getName();
    }

}
