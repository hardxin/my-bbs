package com.xzy.web;

import com.xzy.entity.Article;
import com.xzy.entity.Reply;
import com.xzy.entity.User;
import com.xzy.service.IArticleService;
import com.xzy.service.IReplyService;
import com.xzy.util.License;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 新卓越项目一组
 * @since 2019-05-28
 */
@Controller
public class ArticleController {

    @Autowired
    IArticleService articleService;

    @Autowired
    IReplyService replyService;

    @Autowired
    License license;
    @GetMapping("/article")
    public String article(@RequestParam(value = "id")int id, Model model){
        Article article=articleService.selectArticleById(id);
        List<Reply> replies=replyService.selectAllByArticleId(article.getId());
        model.addAttribute("article", article);
        if(replies.size()>0&&replies.get(0)!=null)
            model.addAttribute("reply0", replies.get(0));
        if(replies.size()>1&&replies.get(1)!=null)
            model.addAttribute("reply1", replies.get(1));
        if(replies.size()>2&&replies.get(2)!=null)
            model.addAttribute("reply2", replies.get(2));
        model.addAttribute("replypage", 1);
        model.addAttribute("articleid", id);
        return "articleDetail";
    }

    @RequestMapping(value = "/addArticle",method = RequestMethod.POST)
    @ResponseBody
    public String addArticle(@RequestParam("addArticleTitle")String title,
                             @RequestParam("addArticleContent")String content,
                             HttpSession session){
        Date date=new Date();
        User user=(User)session.getAttribute("rs_user");
        if(user==null){
            String json="{\"errorCode\":\"0\",\"errorMessage\":\"尚未登陆!!\"}";
            return json;
        }
        if(!license.hasArticlePermission("发布文章", user)){
            String json="{\"errorCode\":\"2\",\"errorMessage\":\"没有该权限\"}";
            return json;
        }
        articleService.addArticle(user.getId(), title, user.getNickname(), date,content);
        String json="{\"errorCode\":\"1\",\"errorMessage\":\"发布成功\"}";
        return json;


    }

    /**
     * 刪除文章
     */
    @PostMapping("/deleteArticle")
    @ResponseBody
    public String editArticle(@RequestParam("articleId") int articleId,
                              @RequestParam("deleteArticleId")int deleteArticleId,
                              HttpSession session){

        User user=(User)session.getAttribute("rs_user");
        if(user==null){
            String json="{\"errorCode\":\"0\",\"errorMessage\":\"尚未登陸\"}";
            return json;
        }
        if(!(license.hasArticlePermission("删除自己文章", user))&&!(license.hasArticlePermission("刪除他人文章", user))){
            String json="{\"errorCode\":\"2\",\"errorMessage\":\"沒有该权限\"}";
            return json;
        }
        if(user.getId()==deleteArticleId) {
            if (!(license.hasArticlePermission("删除自己文章", user))) {
                String json = "{\"errorCode\":\"3\",\"errorMessage\":\"没有删除自己文章权限\"}";
                return json;
            }
        }
            else {
                if (!(license.hasArticlePermission("删除他人文章", user))) {
                    String json = "{\"errorCode\":\"4\",\"errorMessage\":\"没有删除他人文章的权限\"}";
                    return json;
                }

        }
            articleService.deleteArticle(articleId);
            String json="{\"errorCode\":\"1\",\"errorMessage\":\"删除成功\"}";
            return json;

        }

}
