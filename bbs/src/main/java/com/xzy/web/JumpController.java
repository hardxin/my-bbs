package com.xzy.web;

import com.xzy.dto.ShowArticle;
import com.xzy.entity.Article;
import com.xzy.entity.Reply;
import com.xzy.entity.User;
import com.xzy.service.IArticleService;
import com.xzy.service.IReplyService;
import com.xzy.util.ArticleSort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class JumpController {

    @Autowired
    IArticleService articleService;

    @Autowired
    IReplyService replyService;
    @GetMapping("/")
    public String index(){
        System.out.println("进入页面");
        return "index";
    }

    @GetMapping("/toarticle")
    public String toArticle(Model model){
        List<Article> list=articleService.selectAllArticle();
        List<ShowArticle> showArticleList=new ArrayList<ShowArticle>();

        for(Article article:list){
            ShowArticle showArticle=new ShowArticle();
            showArticle.setArticle_id(article.getId());
            showArticle.setArticle_author(article.getAuthor());
            showArticle.setArticle_date(article.getDate());
            showArticle.setArticle_title(article.getTitle());
            List<Reply> replyList=replyService.selectAllByArticleId(article.getId());
            showArticle.setCount(replyList.size()==0?0:replyList.size());
            if(replyList.size()>0) {
                showArticle.setReply_date(replyList.get(0).getDate());
            }
            else
                showArticle.setReply_date(null);
            showArticleList.add(showArticle);
        }
        showArticleList= ArticleSort.articleSort(showArticleList);
        model.addAttribute("list0", showArticleList.get(0));
        model.addAttribute("list1", showArticleList.get(1));
        model.addAttribute("list2", showArticleList.get(2));
        model.addAttribute("page", 1);
        return "article";
    }

    /**
     * 登陆界面
     *
     */
    @GetMapping("/tologin")
    public String login(){
        return "login";
    }

    @GetMapping("/tologout")
    public String logout(HttpSession session){
        session.removeAttribute("rs_user");
        session.invalidate();
        return "index";
    }

    /**
     * 自己文章
     */
    @GetMapping("/tomyarticle")
    public String tomyarticle(HttpSession session, Model model, HttpServletResponse response){
        User user=(User)session.getAttribute("rs_user");
        if(user==null){
         response.setContentType("text/html;character=UTF-8");
            PrintWriter out=null;
            try {
                out=response.getWriter();
                out.flush();
                out.flush();
                out.println("<script>");
                out.println("alert('您当前尚未登陆，请先登录！');");
                out.println("history.back();");
                out.println("</script>");
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                out.close();
            }


            return "article";
        }
        Integer id=user.getId();
        List<Article> articleList=articleService.selectAllArticleByUserId(id);
        List<ShowArticle> list=new ArrayList<ShowArticle>();
        for(Article t:articleList){
            ShowArticle showArticle=new ShowArticle();
            showArticle.setArticle_id(t.getId());
            showArticle.setArticle_author(t.getAuthor());
            showArticle.setArticle_date(t.getDate());
            showArticle.setArticle_title(t.getTitle());
            showArticle.setCount(replyService.selectAllByArticleId(t.getId()).size()==0?0:replyService.selectAllByArticleId(t.getId()).size());
            showArticle.setReply_date(replyService.selectAllByArticleId(t.getId()).size()==0?null:replyService.selectAllByArticleId(t.getId()).get(0).getDate());
            list.add(showArticle);
        }
        list= ArticleSort.articleSort(list);
        if (list.size()>0&&list.get(0)!=null)
            model.addAttribute("list0",list.get(0));
        if (list.size()>1&&list.get(1)!=null)
            model.addAttribute("list1",list.get(1));
        if (list.size()>2&&list.get(2)!=null)
            model.addAttribute("list2",list.get(2));
        model.addAttribute("mypage",1);
        return "myarticle";

    }



}
