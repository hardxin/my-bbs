package com.xzy.web;

import com.xzy.annotation.MyTag;
import com.xzy.entity.Role;
import com.xzy.entity.RolePer;
import com.xzy.entity.User;
import com.xzy.service.IRolePerService;
import com.xzy.service.IRoleService;
import com.xzy.service.IUserService;
import com.xzy.util.ConvertRole;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 新卓越项目一组
 * @since 2019-05-28
 */
@Controller
public class UserController {


   private Logger logger= LoggerFactory.getLogger(this.getClass());
    @Autowired
    IUserService userService;

    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    IRoleService roleService;

    @Autowired
    IRolePerService rolePerService;
    /**
     * 用户的注册登录
     *
     */
    @PostMapping(value = "/register")
    @ResponseBody
    public String register(@RequestParam(value ="nickname",required = true)String nickname,
                           @RequestParam(value ="tel",required = true)String tel,
                           @RequestParam(value ="volidate",required = true)String volidate,
                           @RequestParam(value ="password",required = true)String password){
        User user=userService.selectUserByTel(tel);
        if(!redisTemplate.hasKey(tel)||redisTemplate.opsForValue().get(tel).equals(volidate)){
               logger.info("volidate为"+volidate+redisTemplate.opsForValue().get(tel));
               logger.info("验证码错误");
               String json="{\"errorCode\":\"2\",\"errorMessage\":\"验证码错误了\"}";
               return json;
        }else{
            User u=new User();
            if(user==null){
                logger.info("进入循环");
                 u.setPassword(DigestUtils.md5Hex(password));
                 u.setTel(tel);
                 u.setNickname(nickname);
                 userService.addUser(u);
                 String json="{\"errorCode\":\"1\",\"errorMessage\":\"成功了\"}";
                 return  json;
            }else{
                logger.info("没有进入循环");
                String json="{\"errorCode\":\"0\",\"errorMessage\":\"phone or password is error\"}";
                return json;
            }
        }

    }
    /**
     * 转到注册界面
     */
    @GetMapping("/toregister")
    public String toregister(){
        return "register";
    }

    @GetMapping("/test")
    public String test(){
        return "articleDetail";
    }

    /**
     * 用户登陆
     */
    @RequestMapping(value = "/login",method = RequestMethod.POST)
    @ResponseBody
    public String login(@RequestParam(value = "tel")String tel,
                        @RequestParam(value = "password")String password,
                        HttpServletRequest request,
                        HttpServletResponse response){
        User user=new User();
        user.setTel(tel);
        user.setPassword(password);
        //判断用户是否在数据库里
        User u=userService.selectUserByTel(tel);
        if(u==null){
            String json="{\"errorCode\":\"11\",\"errorMessage\":\"+该用户不存在+\"}";
            return json;
        }
       // User rs_user=userService.se
        User rs_user=userService.selectUser(tel,DigestUtils.md5Hex(password));
        int count=redisTemplate.opsForValue().get("错误登陆"+tel)==null?
                0:(int)redisTemplate.opsForValue().get("错误登陆"+tel);
        if(rs_user!=null&&count<3){
            HttpSession session=request.getSession();
            session.setAttribute("rs_user", rs_user);
            redisTemplate.delete("错误登陆"+tel);
            String json= "{\"errorCode\":\"00\",\"errorMessage\":\"登陆成功！\"}";
            return json;
        }else if(count>=3){
            String json="{\"errorCode\":\"33\",\"errorMessage\":\"您由于错误登陆次数太多，系统已将您的账户锁定，请在三分钟后重新登录！\"}";
            count++;
            redisTemplate.opsForValue().set("错误登陆"+tel, count,180, TimeUnit.SECONDS);
            return json;
        }else{
            String json="{\"errorCode\":\"22\",\"errorMessage\":\"密码输入错误，错误输入三次后您的账户将会被锁定！\"}";
            count++;
            redisTemplate.opsForValue().set("错误登陆"+tel, count,60,TimeUnit.SECONDS);
            return json;
        }
    }

    @MyTag(name = "你是猪")
    @GetMapping("/queryPermission")
    public String query(@RequestParam(value = "id",required = true)int id, Model model){
        Role role=roleService.selectById(id);
        List<Integer> list=rolePerService.selectPermission(role.getId());
        List myPermission= ConvertRole.toList(list);
        model.addAttribute("myRole", role);
        model.addAttribute("myPermission", myPermission);
        return "selectPermission";
    }
}
