package com.xzy.web;

import com.github.pagehelper.PageHelper;
import com.xzy.dto.ShowArticle;
import com.xzy.entity.Article;
import com.xzy.service.IArticleService;
import com.xzy.service.IReplyService;
import com.xzy.util.ArticleSort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class PageController {


    @Autowired
    IArticleService articleService;

    @Autowired
    IReplyService replyService;
    @RequestMapping(value = "/page",method = RequestMethod.GET)
    public String page(@RequestParam("page")int page, Model model){
        int tpage=page;
        List<Article> articleList=articleService.selectAllArticle();
        int rs=articleList.size();
        if(page<1||(page-1)*3>=rs)
            tpage=1;
        List<ShowArticle> list=new ArrayList<ShowArticle>();
        for(Article t:articleList){
            ShowArticle showArticle=new ShowArticle();
            showArticle.setArticle_id(t.getId());
            showArticle.setArticle_author(t.getAuthor());
            showArticle.setArticle_date(t.getDate());
            showArticle.setArticle_title(t.getTitle());
            showArticle.setCount(replyService.selectAllByArticleId(t.getId()).size()==0?0:replyService.selectAllByArticleId(t.getId()).size());
            showArticle.setReply_date(replyService.selectAllByArticleId(t.getId()).size()==0?null:replyService.selectAllByArticleId(t.getId()).get(0).getDate());
            list.add(showArticle);
        }
        list= ArticleSort.articleSort(list);
        System.out.println("page的值为："+tpage);
        if (list.size()>((tpage-1)*3)&&list.get((tpage-1)*3)!=null)
            model.addAttribute("list0",list.get((tpage-1)*3));
        if (list.size()>((tpage-1)*3+1)&&list.get((tpage-1)*3+1)!=null)
            model.addAttribute("list1",list.get((tpage-1)*3+1));
        if (list.size()>((tpage-1)*3+2)&&list.get((tpage-1)*3+2)!=null)
            model.addAttribute("list2",list.get((tpage-1)*3+2));
        model.addAttribute("page",tpage);
        return "article";

    }
}
