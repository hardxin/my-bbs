package com.xzy.web;

import com.xzy.entity.User;
import com.xzy.service.IReplyService;
import com.xzy.util.License;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.Date;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 新卓越项目一组
 * @since 2019-05-28
 */
@Controller
public class ReplyController {


    /**
     * 增加评论
     */

    @Autowired
    License license;

    @Autowired
    IReplyService replyService;
    @RequestMapping(value = "/addReply",method = RequestMethod.POST)
    @ResponseBody
    public String addReply(@RequestParam(value = "articleId")int articleId,
                           @RequestParam(value = "reply_content")String reply_content,
                           HttpSession session){
        Date date=new Date();
        User user=(User)session.getAttribute("rs_user");
        if(user==null){
            String json="{\"errorCode\":\"0\",\"errorMessage\":\"尚未登陆\"}";
            return json;
        }
        if(!license.hasArticlePermission("评论文章", user)){
            String json="{\"errorCode\":\"2\",\"errorMessage\":\"没有该权限\"}";
            return json;
        }

        String replyer=user.getNickname();
        replyService.addReply(articleId, replyer,date, reply_content);
        String json="{\"errorCode\":\"1\",\"errorMessage\":\"评论成功！\"}";
        return json;
    }
}
