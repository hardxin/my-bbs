package com.xzy.web;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.xzy.entity.Role;
import com.xzy.entity.User;
import com.xzy.entity.UserRole;
import com.xzy.service.IRolePerService;
import com.xzy.service.IRoleService;
import com.xzy.service.IUserRoleService;
import com.xzy.service.IUserService;
import com.xzy.util.ConvertRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 新卓越项目一组
 * @since 2019-05-28
 */
@Controller
public class PermissionController {


    @Autowired
    IRoleService roleService;

    @Autowired
    IUserService userService;

    @Autowired
    IUserRoleService userRoleService;

    @Autowired
    IRolePerService rolePerService;
    /**
     * 前往用户权限管理界面
     */
    @RequestMapping(value = "/topermission",method = RequestMethod.GET)
    public String toPermission(HttpSession session, Model model, HttpServletResponse response) {
        User user= (User) session.getAttribute("rs_user");
        if (user==null){
            response.setContentType("text/html; charset=UTF-8"); //转码
            PrintWriter out = null;
            try {
                out = response.getWriter();
            } catch (IOException e) {
                e.printStackTrace();
            }
            out.flush();
            out.println("<script>");
            out.println("alert('您当前尚未登陆，请先登录！');");
            out.println("history.back();");
            out.println("</script>");
            return "article";
        }
        int id = user.getId();
        PageHelper.startPage(0, 3);
        Page<Role> roles = roleService.selectAll();
        Page<User> users = userService.selectAll();
        UserRole userRole = userRoleService.selectByUserId(id);
        Role myrole = roleService.selectById(userRole.getRoleid());
        List<Role> allUserRole = roleService.selectAllRole();
        model.addAttribute("allUserRole", allUserRole);
        List<String> myPermission = ConvertRole.toList(rolePerService.selectPermission(myrole.getId()));
        model.addAttribute("myRole", myrole);
        model.addAttribute("myPermission", myPermission);
        model.addAttribute("allRole", roles);
        if (users.size() > 0 && users.get(0) != null) {
           UserRole userRole1=userRoleService.selectByUserId(users.get(0).getId());
           Role role0=roleService.selectById(userRole1.getRoleid());
           model.addAttribute("user0", users.get(0));
           model.addAttribute("userRole0", role0);
        }
        if (users.size() > 1 && users.get(1) != null) {
            UserRole userRole1=userRoleService.selectByUserId(users.get(1).getId());
            Role role1=roleService.selectById(userRole1.getRoleid());
            model.addAttribute("user1", users.get(1));
            model.addAttribute("userRole1", role1);
        }
        if (users.size() > 2 && users.get(2) != null) {
            UserRole userRole1=userRoleService.selectByUserId(users.get(2).getId());
            Role role1=roleService.selectById(userRole1.getRoleid());
            model.addAttribute("user2", users.get(2));
            model.addAttribute("userRole2", role1);
        }
        if(roles.size()>0&&roles.get(0)!=null)
            model.addAttribute("role0", roles.get(0));
        if(roles.size()>1&&roles.get(1)!=null)
            model.addAttribute("role1", roles.get(1));
        if(roles.size()>2&&roles.get(2)!=null)
            model.addAttribute("role2", roles.get(2));
        model.addAttribute("userPage", 1);
        model.addAttribute("rolepage", 1);
        List<Role> roleList=roleService.selectAllRole();
        model.addAttribute("roles", roleList);
        if(myrole.getLevel()==1)
            return "userPermission";
        return "permission";
    }
}
