package com.xzy;

import com.xzy.annotation.MyTag;
import com.xzy.entity.Article;
import com.xzy.entity.User;
import com.xzy.service.IArticleService;
import com.xzy.service.IUserService;
import com.xzy.web.UserController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.Model;

import java.lang.reflect.Method;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BbsApplicationTests {

    @Autowired
    IArticleService articleService;

    @Autowired
    IUserService userService;
    @Test
    public void contextLoads() {
        List<Article> articles = articleService.selectAllArticle();
        for(Article article:articles)
            System.out.println(article);

    }
    @Test
    public void getUser(){
        User user=userService.selectUser("15877445326", "5201027xin");
        System.out.println(user);
    }

    @Test
    public void hello(){
        try {
            Class<?> myclass = Class.forName("com.xzy.web.UserController");
            Method query = myclass.getMethod("query", int.class, Model.class);
            MyTag annotation = query.getAnnotation(MyTag.class);
            System.out.println(annotation.name());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
