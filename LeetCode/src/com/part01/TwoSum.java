package com.part01;

import java.util.HashMap;

public class TwoSum {

    public int[] solution(int[] array,int target){
        int[] res=new int[2];
        HashMap<Integer,Integer> map=new HashMap<>();
        for(int i=0;i<array.length;i++){
            if(map.containsKey(target-array[i])){
                res[0]=i;
                res[1]=map.get(target-array[i]);
                break;
            }
            map.put(array[i], i);
        }
        return res;
    }
}
