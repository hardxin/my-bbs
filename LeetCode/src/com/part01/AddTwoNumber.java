package com.part01;

public class AddTwoNumber {

    static class ListNode{
        public int val;
        public  ListNode next;
        public  ListNode(int val){
            this.val=val;
        }
    }
    public ListNode addTwoNumbers(ListNode l1,ListNode l2){
        ListNode dummy=new ListNode(-1);
        ListNode cur=dummy;
        int carry=0;
        while(l1!=null||l2!=null){
            int val1=l1==null?0:l1.val;
            int val2=l2==null?0:l2.val;
            int sum=val1+val2+carry;
            carry=sum/10;
            cur.next=new ListNode(sum%10);
            sum=sum/10;
            cur=cur.next;
        }
        if(carry==1)
            cur.next=new ListNode(1);
        return dummy.next;
    }
}
