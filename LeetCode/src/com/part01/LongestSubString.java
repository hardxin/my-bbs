package com.part01;

import java.util.HashSet;

public class LongestSubString {

    public int lengthOfLongestSubstring(String s){
        HashSet<Character> set=new HashSet<>();
        int left=0;
        int right=0;
        int res=0;
        int length=s.length();
        while(right<length){
            if(set.contains(s.charAt(right))) {
                set.add(s.charAt(right++));
                res=Math.max(res, set.size());
            }else
                set.remove(s.charAt(left++));
        }
        return res;
    }
}
